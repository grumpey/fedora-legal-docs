////

Copyright Fedora Project Authors.

SPDX-License-Identifier: CC-BY-SA-4.0

////

= Helpful information about licenses

This information on license information in community project source code is aimed to help Fedora contributors with such tasks as populating the `License:` field in a package spec file and determining whether a license needs to be reviewed for use in Fedora Linux.

== What is the license of a package?

The information about what licenses apply to a project typically can be found in a few places:

* *License files*: The source code may contain a specific file containing just the license text (most often named LICENSE or COPYING, but many other conventions and variant practices exist). These license files are usually, but not always, in the root directory of the source tree. There may be more than one such file, in which case, hopefully, there is information elsewhere in the source tree that explains the applicability of the different licenses. License files may also be present in non-root directories in the source tree, such as in cases where the files in one part of the source tree are licensed under a license that is different from the rest of the source tree.

* *Source files*:  License notices referencing a particular license, or even the full text of relatively short licenses, are often found in source files. License notices may include: standard license notices suggested by the license itself (e.g., GPL, Apache-2.0); short concise statements (e.g., "This file is under the MIT license" or "SPDX-License-Identifier: MIT"); statements that simply refer to the license file that require finding that file (e.g., "See COPYING"); or unclear or ambiguous statements that require further investigation (e.g., "This file is licensed the same as Ruby"). 

* Information about applicable license terms may be present in descriptive form, such as a statement in a README, the project webpage, metadata intended for a downstream packaging system or package repository, or in a file dedicated to explaining the license terms. 

Unfortunately, it is rather common for this sort of information to be inconsistent with or contradict other license
information elsewhere. While it's difficult to give general guidance on this situation, you generally cannot ignore license information that is "closer" to the code (for example, a license notice in a source file) even if it might seem to be contradicted by license information "further" from the code (for example, a license statement in a top-level README or on a project website).




